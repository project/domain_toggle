
/**
 * @file
 * README file for Domain Toggle.
 */

Domain Toggle
Allows domain administrators to make a domain open or closed.

Closed domain:
Forces users to be assigned to a domain in order to view content on that domain.

Open domain:
Anonymous users can view the content.

CONTENTS
--------

1.  Introduction
2.  Installation
3.  Configuration

----
1. Introduction

This module is a modified version of the Domain Strict module.

The domain strict module alters the behaviour for the Domain Access
module on all domains. This module enables the administrator to choose
what domains will have domain strict access rules, and what domains will
have normal domain access rules. 

----
2. Installation

To install, unpack the module to your modules folder, and simply enable the module at Admin > Build > Modules.

The database table {domain_toggle} is installed to have a separate storage for this module.

----
3.  Configuration
You toggle which domains are open and which are closed in the domain edit form.
You can access the domain edit form for each domain at Admin > Build > Domain > View.

From domain_strict readme file:
Since this module changes the way the Domain Access module behaves,
you may want to alter some of the default Domain Access settings.

Specifically, read up on the 'Special page requests' section of the main
README.

If you want this module to restrict all content viewing, you should:

  1) Set the 'Search settings' to the default value:
      'Search content for the current domain only'

  2) Clear out any rules in the 'Special page requests' settings.

Both these options allow users to see all nodes on specific pages on
any active domain.